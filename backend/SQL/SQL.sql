CREATE TABLE admin(
   Id_admin SERIAL,
   username VARCHAR(50) ,
   password VARCHAR(50) ,
   updateDate TIMESTAMP,
   PRIMARY KEY(Id_admin)
);

INSERT INTO admin (username,password) VALUES('Nomenandrianina','antonio123456');

CREATE TABLE category(
   Id_category SERIAL,
   name VARCHAR(50) ,
   descirpiton TEXT,
   creation_date TIMESTAMP,
   update_date TIMESTAMP,
   PRIMARY KEY(Id_category)
);

CREATE TABLE region(
   Id_region SERIAL,
   nom VARCHAR(50) ,
   descirption TEXT,
   posting_date TIMESTAMP,
   update_date TIMESTAMP,
   PRIMARY KEY(Id_region)
);

INSERT INTO region (nom,descirption,posting_date) VALUES('Diana','Antsiranana',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Sava','Antsiranana',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Itasy','Antananarivo',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Analamanga','Antananarivo',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Vakinankaratra','Antananarivo',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Bongolava','Antananarivo',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Sofia','Mahajanga',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Boeny','Mahajanga',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Betsiboka','Mahajanga',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Melaky','Mahajanga',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Alaotra-Mangoro','Toamasina',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Atsinanana','Toamasina',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Analanjirofo','Toamasina',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Amoron"i Mania','Fianarantsoa',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Haute Matsiatra','Fianarantsoa',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Vatovavy Fitovinany','Fianarantsoa',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Atsimo-Atsinanana','Fianarantsoa',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Ihorombe','Fianarantsoa',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Menabe','Toliara',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Atsimo-Andrefana','Toliara',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Androy','Toliara',now());
INSERT INTO region (nom,descirption,posting_date) VALUES('Anôsy','Toliara',now());

CREATE TABLE subcategory(
   Id_subcategory SERIAL,
   name VARCHAR(50) ,
   creation_date TIMESTAMP,
 TIMESTAMP,
   Id_category INTEGER NOT NULL,
   PRIMARY KEY(Id_subcategory),
   FOREIGN KEY(Id_category) REFERENCES category(Id_category)
);

CREATE TABLE users(
   Id_users SERIAL,
   fullname VARCHAR(50) ,
   email VARCHAR(50) ,
   password VARCHAR(50) ,
   address VARCHAR(50) ,
   reg_date TIMESTAMP,
   updation_date TIMESTAMP,
   status VARCHAR(50) ,
   PRIMARY KEY(Id_users)
);

CREATE TABLE complaints(
   Id_complaints SERIAL,
   complaint_file VARCHAR(50) ,
   details TEXT,
   reg_date TIMESTAMP,
   status VARCHAR(50) ,
   updation_date TIMESTAMP,
   Id_region INTEGER,
   Id_category INTEGER NOT NULL,
   Id_users INTEGER NOT NULL,
   PRIMARY KEY(Id_complaints),
   FOREIGN KEY(Id_region) REFERENCES region(Id_region),
   FOREIGN KEY(Id_category) REFERENCES category(Id_category),
   FOREIGN KEY(Id_users) REFERENCES users(Id_users)
);

CREATE TABLE userlog(
   Id_userlog SERIAL,
   user_ip VARCHAR(50) ,
   login_time TIMESTAMP,
   logout_time TIMESTAMP,
   status VARCHAR(50) ,
   Id_users INTEGER NOT NULL,
   PRIMARY KEY(Id_userlog),
   FOREIGN KEY(Id_users) REFERENCES users(Id_users)
);

CREATE TABLE complaint_remark(
   Id_complaint_remark SERIAL,
   status VARCHAR(50) ,
   remark TEXT,
   remark_date VARCHAR(50) ,
   Id_complaints INTEGER NOT NULL,
   PRIMARY KEY(Id_complaint_remark),
   FOREIGN KEY(Id_complaints) REFERENCES complaints(Id_complaints)
);
