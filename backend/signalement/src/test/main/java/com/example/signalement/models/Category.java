package com.example.signalement.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the category database table.
 * 
 */
@Entity
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_category")
	private int id_category;

	@Column(name="codeSignal")
	private String codeSignal;

	@Column(name="name")
	private String name;

	@OneToMany( targetEntity=Signalement.class )
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private List signalement;



	public List getSignalement() {
		return signalement;
	}

	public void setSignalement(List signalement) {
		this.signalement = signalement;
	}

	public Category() {
	}

	public int getId_category() {
		return this.id_category;
	}

	public void setId_category(int id_category) {
		this.id_category = id_category;
	}



	public String getCodeSignal() {
		return codeSignal;
	}

	public void setCodeSignal(String codeSignal) {
		this.codeSignal = codeSignal;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}



	
	public Category(String code,String name) {
		this.codeSignal = code;
		this.name = name;
	}
	
	public Category(int id_category,String code,String name) {
		this.id_category = id_category;
		this.codeSignal = code;
		this.name = name;
	}

}