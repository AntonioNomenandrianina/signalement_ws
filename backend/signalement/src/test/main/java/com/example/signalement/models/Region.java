package com.example.signalement.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the region database table.
 * 
 */
@Entity
@NamedQuery(name="Region.findAll", query="SELECT r FROM Region r")
public class Region implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_region")
	private int id_region;

	@Column(name="region")
	private String region;
	

	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}



	@OneToMany(targetEntity=Signalement.class )
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private List signalement;

	public List getSignalement() {
		return signalement;
	}

	public void setSignalement(List signalement) {
		this.signalement = signalement;
	}


	public int getId_region() {
		return this.id_region;
	}

	public void setId_region(int id_region) {
		this.id_region = id_region;
	}

	public Region() {
	}
	
	public Region(int id_region,String region) {
		this.id_region = id_region;
		this.region = region;
	}
	




}