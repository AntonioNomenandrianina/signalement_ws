package com.example.signalement.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.signalement.models.Region;
import com.example.signalement.repository.RegionRepository;
import com.example.signalement.staus.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin(origins="*",allowedHeaders="*")
public class RegionController {
	private int status_error = 404;
    private int status_success = 200;
    
	@Autowired
	private RegionRepository regionRepository;
	
	
	@GetMapping("region/all")
    public Response getAllUtilisateur() {
		Response resp = new Response();
        try {
        	Iterable<Region> list =   regionRepository.findAll();
        	resp = new Response(status_success,"Success",list);
        }
        catch(Exception ex) {
        	System.out.print(ex);
        	resp = new Response(status_error,"Serveur error");
        }
        return resp;
    } 
	
	@ResponseBody
	@CrossOrigin(origins="*",allowedHeaders="*")
	@PostMapping("create/region")
    public Response saveCategory(@RequestParam("region") String region) throws IOException {
		Response resp = new Response();
		Region newCategory = new ObjectMapper().readValue(region, Region.class);
        try {
        	regionRepository.save(newCategory);
        	resp = new Response(status_success,"Success");
        }
        catch(Exception ex) {
        	resp = new Response(status_error,"Serveur error");
        }
        return resp;
    } 
}
