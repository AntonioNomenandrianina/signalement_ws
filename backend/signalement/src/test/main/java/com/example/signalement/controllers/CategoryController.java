package com.example.signalement.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.signalement.models.Category;
import com.example.signalement.models.Region;
import com.example.signalement.repository.CategorieRepository;
import com.example.signalement.staus.Response;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
@CrossOrigin(origins="*",allowedHeaders="*")
public class CategoryController {
	private int status_error = 404;
    private int status_success = 200;
    
	@Autowired
	private CategorieRepository categorieRepository;
	
	@ResponseBody
	@CrossOrigin(origins="*",allowedHeaders="*")
	@PostMapping("create/categorie")
    public Response saveCategory(@RequestParam("category") String category) throws IOException {
		Response resp = new Response();
		Category newCategory = new ObjectMapper().readValue(category, Category.class);
        try {
        	categorieRepository.save(newCategory);
        	resp = new Response(status_success,"Success");
        }
        catch(Exception ex) {
        	resp = new Response(status_error,"Serveur error");
        }
        return resp;
    } 
	
	@GetMapping("category/all")
    public Response getAllUtilisateur() {
		Response resp = new Response();
        try {
        	List<Category> list =  (List<Category>) categorieRepository.findAll();
        	resp = new Response(status_success,"Success",list);
        }
        catch(Exception ex) {
        	ex.printStackTrace();
        	resp = new Response(status_error,"Serveur error");
        }
        return resp;
    } 
}
