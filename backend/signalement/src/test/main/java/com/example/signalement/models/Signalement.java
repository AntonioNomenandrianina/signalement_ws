package com.example.signalement.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@NamedQuery(name="Signalement.findAll", query="SELECT s FROM Signalement s")
public class Signalement implements Serializable{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_signalement;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	private User users;
	
	//bi-directional many-to-one association to Region
	@ManyToOne(fetch=FetchType.LAZY)
	private Region region;

	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY)
	private Category category;
	
	@Column(name="description")
	private String description;
	
	@Column(name="photo")
	private String photo;
	
	@Column(name="status", columnDefinition = "INTEGER DEFAULT 0")
	private int status;
	
	@Column(name = "START_DATE", columnDefinition = "DATE DEFAULT CURRENT_DATE")
	private Date date_creation;
	
	@Column(name= "latitudesignalement", columnDefinition = "NUMERIC")
	private double latitudesignalement;
	
	@Column(name= "longitudesignalement", columnDefinition = "NUMERIC")
	private double longitudesignalement;


	public double getLatitudesignalement() {
		return latitudesignalement;
	}

	public void setLatitudesignalement(double latitudesignalement) {
		this.latitudesignalement = latitudesignalement;
	}

	public double getLongitudesignalement() {
		return longitudesignalement;
	}

	public void setLongitudesignalement(double longitudesignalement) {
		this.longitudesignalement = longitudesignalement;
	}

	public User getUsers() {
		return users;
	}

	public void setUsers(User users) {
		this.users = users;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getDate_creation() {
		return date_creation;
	}

	public void setDate_creation(Date date_creation) {
		this.date_creation = date_creation;
	}

	public Signalement() {
		
	}
	
	public int getId_signalement() {
		return this.id_signalement;
	}

	public void setId_signalement(int id_signalement) {
		this.id_signalement = id_signalement;
	}
	
	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public Signalement(Region region,Category category) {
		this.region = region;
		this.category = category;
	}
	




}
