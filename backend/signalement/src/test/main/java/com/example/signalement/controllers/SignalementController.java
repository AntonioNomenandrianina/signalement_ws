package com.example.signalement.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.signalement.models.Category;
import com.example.signalement.models.Region;
import com.example.signalement.models.Signalement;
import com.example.signalement.repository.SignalementRepository;
import com.example.signalement.staus.Response;

@RestController
@CrossOrigin(origins="*",allowedHeaders="*")
public class SignalementController {
	private int status_error = 404;
    private int status_success = 200;
    
	@Autowired
	private SignalementRepository signalementRepository;
	
	@ResponseBody
	@CrossOrigin(origins="*",allowedHeaders="*")
	@PostMapping("create/signalement")
    public Response saveSignalement(@RequestParam("region") String region,@RequestParam("category") String category) throws IOException {
		Response resp = new Response();
		int id_region = Integer.parseInt(region);
		int id_category = Integer.parseInt(category);
		Region newRegion = new Region(id_region,"");
		Category newCategory = new Category(id_category,"","");
		Signalement signale = new Signalement(newRegion,newCategory);
        try {
        	signalementRepository.save(signale);
        	resp = new Response(status_success,"Success");
        }
        catch(Exception ex) {
        	resp = new Response(status_error,"Serveur error");
        }
        return resp;
    } 
	
	@GetMapping("signalement/all")
    public Response getAllSignalement() {
		Response resp = new Response();
        try {
        	List<Signalement> list =  (List<Signalement>) signalementRepository.findAll();
        	resp = new Response(status_success,"Success",list);
        }
        catch(Exception ex) {
        	resp = new Response(status_error,"Serveur error");
        }
        return resp;
    } 
	
	@GetMapping("get/signalement/region/{id}")
    public Response getRegionSignalement(@PathVariable String id) {
		Response resp = new Response();
		int idnew = Integer.parseInt(id);
		Region reg = new Region(idnew,"");
        try {
        	List<Signalement> list =  (List<Signalement>) signalementRepository.findByRegion(reg);
        	resp = new Response(status_success,"Success",list);
        }
        catch(Exception ex) {
        	System.out.print(ex);
        	resp = new Response(status_error,"Serveur error");
        }
        return resp;
    }
	
	@GetMapping("/see/signalement/{id}")
	public Response getByIDSignalement(@PathVariable String id) throws ClassNotFoundException, Exception {
	  Response resp = new Response();
	  int idnew = Integer.parseInt(id);
	  	try {
	  		Optional<Signalement> list =   signalementRepository.findById(idnew);
	  		resp = new Response(status_success,"Signalement found",list);
	  		
	  	}catch(Exception ex) {
	  		resp = new Response(status_error,"Signalement not found");
	  	}
	return resp;
   }
	
	@GetMapping("/delete/signalement/{id}")
	public Response DeleteByIDSignalement(@PathVariable String id) throws ClassNotFoundException, Exception {
	  Response resp = new Response();
	  int idnew = Integer.parseInt(id);
	  	try {
	  		signalementRepository.deleteById(idnew);
	  		resp = new Response(status_success,"Signalement Deleted");
	  		
	  	}catch(Exception ex) {
	  		resp = new Response(status_error,"Signalement not Deleted");
	  	}
	return resp;
   }
	
	@PostMapping("/update/signalement")
	public Response updateSignalement(@RequestParam("id_signalement") String id,@RequestParam("status") String status) {
		Response resp = new Response();
		System.out.println(id);
		System.out.println(status);
		int idnew = Integer.parseInt(id);
		int _status = Integer.parseInt(status);
		try {
			Optional<Signalement> signalementData = signalementRepository.findById(idnew);
			if (signalementData.isPresent()) {
		      Signalement _signalement = signalementData.get();
		      _signalement.setStatus(_status);
		      signalementRepository.save(_signalement);
		      resp = new Response(status_success,"Signalement Update Successffuly");
			} else {
			   resp = new Response(500,"Signalement Update Error");
			}
		}catch(Exception ex) {
	  		resp = new Response(status_error,"Signalement not found");
	  	}
	    return resp;
	 }
	
	@GetMapping("/detail/signalement/{latitude}/{longitude}")
	public Response DetailSignalement(@PathVariable String latitude,@PathVariable  String longitude) {
		Response resp = new Response();
		double _latitude = Double.parseDouble(latitude);
		double _longitude = Double.parseDouble(longitude);
		try {
			Signalement signalementData = signalementRepository.findByLatitudesignalementAndLongitudesignalement(_latitude,_longitude);
		    resp = new Response(status_success,"Signalement Update Successffuly",signalementData);
		}catch(Exception ex) {
	  		resp = new Response(status_error,"Signalement not found");
	  	}
	    return resp;
	 }
	
}
