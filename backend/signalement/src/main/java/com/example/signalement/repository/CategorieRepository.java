package com.example.signalement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.signalement.models.Admin;
import com.example.signalement.models.Category;

@Repository
public interface CategorieRepository extends CrudRepository<Category, Integer>{

}
