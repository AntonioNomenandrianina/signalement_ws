package com.example.signalement.models;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_users")
	private int id_users;

	@Column(name="nom")
	private String nom;

	@Column(name="email")
	private String email;


	@Column(name="password")
	private String password;

	
	 @OneToMany( targetEntity=Signalement.class )
	 @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	 private List<Signalement> signalement;
	


	public List<Signalement> getSignalement() {
		return signalement;
	}

	public void setSignalement(List<Signalement> signalement) {
		this.signalement = signalement;
	}

	public User() {
	}

	public int getId_users() {
		return this.id_users;
	}

	public void setId_users(int id_users) {
		this.id_users = id_users;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User(int id_users, String nom, String email, String password) {
		super();
		this.id_users = id_users;
		this.nom = nom;
		this.email = email;
		this.password = password;
	}



}