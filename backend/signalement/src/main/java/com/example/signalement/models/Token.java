package com.example.signalement.models;

public class Token {
	public int id_utilisateur;
	public String username;
	public String email;
	public String  token;
	public int getId_utilisateur() {
		return id_utilisateur;
	}
	public void setId_utilisateur(int id_utilisateur) {
		this.id_utilisateur = id_utilisateur;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public Token(int id_u,String name,String email,String token) {
		this.id_utilisateur = id_u;
		this.username = name;
		this.email = email;
		this.token = token;
	}
}
