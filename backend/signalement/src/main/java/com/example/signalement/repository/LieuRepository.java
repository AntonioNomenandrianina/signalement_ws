package com.example.signalement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.signalement.models.Lieu;
import com.example.signalement.models.Region;

@Repository
public interface LieuRepository extends CrudRepository<Lieu, Integer>{
	public  Lieu findById(int id);
}