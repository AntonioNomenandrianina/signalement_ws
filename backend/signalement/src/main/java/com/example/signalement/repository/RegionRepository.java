package com.example.signalement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.signalement.models.Region;

@Repository
public interface RegionRepository extends CrudRepository<Region, Integer>{

}
