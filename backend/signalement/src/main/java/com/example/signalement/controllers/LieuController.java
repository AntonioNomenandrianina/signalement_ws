package com.example.signalement.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.signalement.models.Lieu;
import com.example.signalement.repository.LieuRepository;
import com.example.signalement.status.Response;

@RestController
@CrossOrigin(origins="*",allowedHeaders="*")
public class LieuController {
	private int status_error = 404;
    private int status_success = 200;
    
	@Autowired
	private LieuRepository lieuRepository;
	
	@GetMapping("lieu/all")
    public Response getAllUtilisateur() {
		Response resp = new Response();
        try {
        	Iterable<Lieu> list =   lieuRepository.findAll();
        	resp = new Response(status_success,"Success",list);
        }
        catch(Exception ex) {
        	System.out.print(ex);
        	resp = new Response(status_error,"Serveur error");
        }
        return resp;
    } 
}
