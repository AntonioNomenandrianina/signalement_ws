package com.example.signalement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.signalement.models.Region;
import com.example.signalement.models.Signalement;

@Repository
public interface SignalementRepository extends CrudRepository<Signalement, Integer>{
	public Signalement findByLatitudesignalementAndLongitudesignalement(double latitude,double longitude);
	public List<Signalement> findByRegion(Region id);
}
