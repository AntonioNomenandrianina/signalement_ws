package com.example.signalement.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.signalement.models.Admin;
import com.example.signalement.models.Token;
import com.example.signalement.models.User;
import com.example.signalement.repository.AdminRepository;
import com.example.signalement.repository.UserRepository;
import com.example.signalement.status.Response;
import com.example.utils.JwtTokenUtil;

@RestController
@CrossOrigin(origins="*",allowedHeaders="*")
public class UserController {
	private int status_error = 404;
    private int status_success = 200;

    
	@Autowired
    private UserRepository utilisateurRepository;
	
	@Autowired
    private AdminRepository adminRepository;
	

	@GetMapping("admin/all")
    public List<Admin> getAllUtilisateur() {
        return (List<Admin>) adminRepository.findAll();
    }
	
	@CrossOrigin(origins="*",allowedHeaders="*")
	@PostMapping("login/admin")
	public Response loginAdmin(@RequestBody Admin data) {
		Admin log = adminRepository.findByEmailAndPassword(data.getEmail(),data.getPassword());
		Response resp = new Response();
		JwtTokenUtil jwttoken = new JwtTokenUtil();
		if(log == null) {
			resp = new Response(status_error,"Identifiant incorrecte!");
		}else {
			String token = jwttoken.generateToken(log);
			System.out.println(token);
			Token reponse = new Token(log.getId_admin(),log.getEmail(),log.getEmail(),token);
			resp = new Response(status_success,"Connexion réussie!",reponse);
		}
		return resp;
	}
	
	@CrossOrigin(origins="*",allowedHeaders="*")
	@PostMapping("login/user")
	public Response loginUser(@RequestBody User data) {
		User log = utilisateurRepository.findByEmailAndPassword(data.getEmail(),data.getPassword());
		Response resp = new Response();
		JwtTokenUtil jwttoken = new JwtTokenUtil();
		if(log == null) {
			resp = new Response(status_error,"Identifiant incorrecte!");
		}else {
			String token = jwttoken.generateTokenUser(log);
			System.out.println(token);
			Token reponse = new Token(log.getId_users(),log.getEmail(),log.getEmail(),token);
			resp = new Response(status_success,"Connexion réussie!",reponse);
		}
		return resp;
	}

}
