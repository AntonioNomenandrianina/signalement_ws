package com.example.signalement.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.signalement.models.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
	public User findByEmailAndPassword(String usermail,String userpassword);
}
