package com.example.signalement.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name="Lieu.findAll", query="SELECT l FROM Lieu l")
public class Lieu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_lieu;
	
	@Column(name="nom" ,columnDefinition="VARCHAR(250)", insertable = false,updatable = false)
	private String nom;
	
	@Column(name="latitude",columnDefinition="NUMERIC", insertable = false,updatable = false)
	private double latitude;
	
	@Column(name="longitude",columnDefinition="NUMERIC", insertable = false,updatable = false)
	private double longitude;
	

	public int getId_lieu() {
		return id_lieu;
	}

	public void setId_lieu(int id_lieu) {
		this.id_lieu = id_lieu;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
