package com.example.signalement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.signalement.models.Admin;


@Repository
public interface AdminRepository extends CrudRepository<Admin, Integer>{
	public Admin findByEmailAndPassword(String email,String password);
}
